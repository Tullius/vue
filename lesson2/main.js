Vue.component('typemovie', {
    props: ['item'],
    template: `
    <label>
        <input
            type="checkbox"
            @click="clickName"
        >
        {{item.typeName}}
    </label>
`,
    methods: {
        clickName() {
            this.item.value = !this.item.value;
        }
    }
});

Vue.component('btn', {
    props: {
        name: String
    },
    template: '<button>{{ name }}</button>',
});

Vue.component('error', {
    template: '<div class="center">Произошла ошибка</div>'
});
Vue.component('notfound', {
    template: '<div class="center">Ничего не найдено</div>'
});
Vue.component('loading', {
    template: '<div class="center">Loading...</div>'
});
Vue.component('posts', {
    props: ['item'],
    template: `
<div class="col-md-3">
                <div class="card">
                <img :src="item.image_url" class="card-img-top" alt="">
                  <div class="card-body">
                    <h5 class="card-title">{{item.title}}</h5>
                    <p class="card-text">Type: {{item.type}}<br> Rating: {{item.score}}<br>Date start: {{item.start_date}}</p>
                    <a :href="item.url" target="_blank" class="btn btn-primary">More info</a>
                  </div>
                </div>
            </div>
            `,
});
const newApp = new Vue({
    el: '#app',
    data: {
        news: [],
        loading: true,
        error: false,
        search: '',
        movieType: [
            { typeName: 'TV', value: false },
            { typeName: 'Movie', value: false },
            { typeName: 'OVA', value: false }
        ],
        newsLength: 'number',
        deleteNews: false,
        downloadNews: false
    },
    created: function() {
        this.getMovies();
    },
    computed:{
        filteredMovies: function(){
            let self = this;
            let news = this.news;
            let search = this.search;
            news =  news.filter(function (elem) {
                let cond = false;
                if(search==='') cond = true;
                else cond = elem.title.indexOf(search) > -1;
                self.movieType.forEach((item) => {
                    if(item.value) {
                        if(cond && elem.type !== item.typeName) cond = false;
                    };
                });
                return cond;
            });
            self.newsLength = news.length;
            return news;
        },
    },
    methods: {
        deleteMovies: function() {
            this.news = [];
        },
        getMovies: function() {
            let self = this;
            self.loading = true;
            setTimeout(() => {
                self.loading = false
            }, 2000);
            fetch('https://api.jikan.moe/v3/top/anime')
                .then(response => response.json())
                .then(data => {
                    if('top' in data) {
                        self.news = data.top;
                        this.filteredMovies
                    } else {
                        self.error = true;
                    }
                })
                .catch(() => self.error = true);
        },
    },
});


var app = new Vue({
    el: '#app',
    data: {
        index: 1,
        color: 'white',
        tabs: [
            {
                name: 'Film 1',
                activetab: 1,
                actors: [
                    { actor: 'Actor 1' },
                    { actor: 'Actor 2' },
                    { actor: 'Actor 3' },
                ],
                color: 'blue',
            },
            {
                name: 'Film 2',
                activetab: 2,
                actors: [
                    { actor: 'Actor 4' },
                    { actor: 'Actor 5' },
                    { actor: 'Actor 6' },
                ],
                color: 'red',
            },
            {
                name: 'Film 3',
                activetab: 3,
                actors: [
                    { actor: 'Actor 7' },
                    { actor: 'Actor 8' },
                    { actor: 'Actor 9' },
                ],
                color: 'green',
            },
        ],
    }
});
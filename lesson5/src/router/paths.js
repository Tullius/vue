export default [
    {
        path: '/',
        name: 'home',
        components: {
            default: () => import('../views/router/Home.vue'),
            addNav: () => import('../views/router/Nav.vue')
        }
    },
    {
        path: '/about',
        name: 'about',
        components: {
            default: () => import('../views/router/Home.vue'),
            addNav: () => import('../views/router/Nav.vue')
        }
    },
    {
        path: '/artist-list',
        name: 'artistList',
        components: {
            default: () => import('../page/ArtistList.vue'),
            addNav: () => import('../views/router/Nav.vue')
        },
    },
    {
        path: '/artist-info/',
        name: 'artistInfo',
        components: {
            default: () => import('../page/ArtistInfo.vue'),
            addNav: () => import('../views/router/Nav.vue')
        },
    },
    {
        path: '/artist-album/',
        name: 'artistAlbum',
        components: {
            default: () => import('../page/ArtistAlbum.vue'),
            addNav: () => import('../views/router/Nav.vue')
        },
    },
    {
        path: '/composition/',
        name: 'composition',
        components: {
            default: () => import('../page/Composition.vue'),
            addNav: () => import('../views/router/Nav.vue')
        },
    },
]
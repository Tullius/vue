import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    totalArtists: [],
  },
  mutations: {
    SET_ARTISTS(state, payload) {
      state.totalArtists = payload;
    },
    SET_RATING(state, payload) {
      let totalArtists = state.totalArtists;
      let index = totalArtists[payload.artistId].album.indexOf(payload.albumCard);
      totalArtists[payload.artistId].album[index].rating = payload.rating;
      state.totalArtists = totalArtists;
    },

  },
  actions: {
    async GET_ARTISTS({ commit }) {
      let resp = await fetch('http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2');
      commit('SET_ARTISTS', await resp.json());
    },
    async SET_RATING({ commit }, rating) {
      commit('SET_RATING', rating);
    }
  },
  methods: {

  },
})
